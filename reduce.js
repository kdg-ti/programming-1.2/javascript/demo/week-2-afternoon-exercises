let arrays = [[1, 2, 3], [4, 5], [6]]


let reducing
	reducing = arrays.reduce((result, current) =>  result.concat(current))
// using spread instead of concat
 reducing = arrays.reduce((result, current) =>  [...result,...current]
)

console.log(reducing)